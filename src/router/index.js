import Vue from 'vue'
import Router from 'vue-router'
import ambassador from '@/components/Ambassadors/ambassador.vue'
import ambassadors from '@/components/Ambassadors/ambassadors.vue'
import event from '@/components/Events/event.vue'
import eventspg from '@/components/Events/eventspg.vue'
import eventsd from '@/components/Events/eventsd.vue'
import viewap from '@/components/Ambassadors/viewap.vue'
import markA from '@/components/Ambassadors/markA.vue'
import Sevent from '@/components/Events/Sevent.vue'
import approvedu from '@/components/Ambassadors/approvedu.vue'
import pendingu from '@/components/Ambassadors/pendingu.vue'
import patchUsers from '@/components/Ambassadors/patchUsers.vue'
import singleu from '@/components/Ambassadors/singleu.vue'
import createEvent from '@/components/Events/createEvent.vue'
import attach from '@/components/Ambassadors/attach.vue'
import dettach from '@/components/Ambassadors/dettach.vue'
import addassignment from '@/components/Events/addassignment.vue'
import allassign from '@/components/Events/allassign.vue'
import socialaccount from '@/components/SocialMedia/socialaccount.vue'
import socialm from '@/components/SocialMedia/socialm.vue'
import allsocial from '@/components/SocialMedia/allsocial.vue'
import userid from '@/components/Ambassadors/userid.vue'
import ambassadorspg from '@/components/Ambassadors/ambassadorspg.vue'
import SingleSocialMedia from '@/components/SocialMedia/SingleSocialMedia'
import feedback from '@/components/feedback'
import feedback2 from '@/components/feedback2'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/ambassadorspg',
      name: 'ambassadorspg',
      component: ambassadorspg
    },
    {
      path: '/ambassadors/:user_public_id',
      name: 'ambassadors',
      component: ambassadors
    },
    {
      path: '/ambassador',
      name: 'ambassador',
      component: ambassador
    },
    {
      path: '/allsocial',
      name: 'allsocial',
      component: allsocial
    },
    {
      path: '/single-social-media/:social_id',
      name: 'SingleSocialMedia',
      component: SingleSocialMedia
    },
    {
      path: '/singleu/:social_id',
      name: 'singleu',
      component: singleu
    },
    {
      path: '/pendingu',
      name: 'pendingu',
      component: pendingu
    },
    {
      path: '/viewap',
      name: 'viewap',
      component: viewap
    },
    {
      path: '/Sevent/:social_id',
      name: 'Sevent',
      component: Sevent
    },
    {
      path: '/approvedu',
      name: 'approvedu',
      component: approvedu
    },
    {
      path: '/eventspg',
      name: 'eventspg',
      component: eventspg
    },
    {
      path: '/markA',
      name: 'markA',
      component: markA
    },
    {
      path: '/patchUsers',
      name: 'patchUsers',
      component: patchUsers
    },
    {
      path: '/eventsd',
      name: 'eventsd',
      component: eventsd
    },
    {
      path: '/event',
      name: 'event',
      component: event
    },
    {
      path: '/createEvent',
      name: 'createEvent',
      component: createEvent
    },
    {
      path: '/attach',
      name: 'attach',
      component: attach
    },
    {
      path: '/dettach',
      name: 'dettach',
      component: dettach
    },
    {
      path: '/addassignment',
      name: 'addassignment',
      component: addassignment
    },
    {
      path: '/allassign',
      name: 'allassign',
      component: allassign
    },
    {
      path: '/socialm',
      name: 'socialm',
      component: socialm
    },
    {
      path: '/socialaccount',
      name: 'socialaccount',
      component: socialaccount
    },
    {
      path: '/userid',
      name: 'userid',
      component: userid
    },
    {
      path: '/feedback',
      name: 'feedback',
      component: feedback
    },
    {
      path: '/feedback2',
      name: 'feedback2',
      component: feedback2
    }
  ],
  mode: 'history'
})
