export default {
  POST_BASIC_ON_USER_ACCOUNT (state, data) {
    state.post_basic_on_user_account = data
  },
  POST_BASIC_ON_PROFILE (state, data) {
    state.post_basic_on_profile = data
  },
  GET_ALL_USERS (state, data) {
    state.get_all_users = data
  },
  GET_ALL_APPROVED (state, data) {
    state.get_all_approved = data
  },
  GET_ALL_PENDING (state, data) {
    state.get_all_pending = data
  },
  GET_ALL_SINGLE_USER (state, data) {
    state.get_all_single_user = data
  },
  PATCH_USER (state, data) {
    state.patch_user = data
  },
  DELETE_USER (state, data) {
    state.delete_user = data
  }
}
