import mutations from './mutations'
import actions from './actions'

const state = {
  post_basic_on_user_account: [],
  post_basic_on_profile: [],
  get_all_users: [],
  get_all_approved: [],
  get_all_pending: [],
  get_all_single_user: [],
  patch_user: [],
  delete_user: []
}
export default {
  state, mutations, actions
}
