import Vue from 'vue'
import router from '../../router/index'

export default {
  post_basic_on_user_account (context, data) {
    Vue.http.post('http://keatrading.co.ke:6003/users/apply', data).then(function (response) {
      router.push({
        name: 'viewap'
      })
      Vue.data('post_basic_on_user_account')
      context.dispatch('POST_BASIC_ON_USER_ACCOUNT', response.body.data)
    })
  },
  post_basic_on_profile (context, data) {
    Vue.http.post('http://keatrading.co.ke:6003/users/apply', data).then(function (response) {
      router.push({
        name: 'ambassadorspg'
      })
      Vue.data('post_basic_on_profile')
      context.dispatch('POST_BASIC_ON_PROFILE', response.body.data)
    })
  },
  get_all_users (context) {
    Vue.http.get('http://keatrading.co.ke:6003/users/view').then(function (response) {
      context.commit('GET_ALL_USERS', response.body.data)
    })
  },
  get_all_approved (context) {
    Vue.http.get('http://keatrading.co.ke:6003/users/view/approved').then(function (response) {
      context.commit('GET_ALL_APPROVED', response.body.data)
    })
  },
  get_all_pending (context) {
    Vue.http.get('http://keatrading.co.ke:6003/users/view/pending').then(function (response) {
      context.commit('GET_ALL_PENDING', response.body.data)
    })
  },
  get_all_single_user (context, publicId) {
    Vue.http.get('http://keatrading.co.ke:6003/users/view/' + publicId).then(function (response) {
      context.commit('GET_ALL_SINGLE_USER', response.body.data)
    })
  },
  patch_user (context) {
    Vue.http.patch('http://keatrading.co.ke:6003/users/approve').then(function (response) {
      context.commit('PATCH_USER', response.body.data)
    })
  },
  delete_user (context) {
    Vue.http.patch('http://keatrading.co.ke:6003/users/view/user_id').then(function (response) {
      context.commit('DELETE_USER', response.body.data)
    })
  }
}
