export default {
  GET_ALL_EVENTS (state, data) {
    state.all_events = data
  },
  GET_SINGLE_EVENT (state, data) {
    state.get_single_event = data
  },
  UPDATE_SINGLE_EVENT (state, data) {
    state.update_event = data
  },
  DELETE_SINGLE_EVENT (state, data) {
    state.delete_single_event = data
  },
  GET_ALL_ASSIGMENT (state, data) {
    state.all_assigment = data
  },
  ATTACH_AMBASSADOR_TO_EVENT (state, data) {
    state.attach_ambassador_to_event = data
  },
  DETTACH_AMBASSADOR_TO_EVENT (state, data) {
    state.dettach_ambassador_to_event = data
  },
  GET_ALL_EVENTS_AN_AMBASSADOR (state, data) {
    state.get_all_events_an_ambassador = data
  },
  MARK_AN_AMBASSADOR_FOR_AN_EVENT (state, data) {
    state.mark_an_ambassador_for_an_event = data
  }
}
