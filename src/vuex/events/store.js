import mutations from './mutations'
import actions from './actions'

const state = {
  all_events: [],
  event: [],
  single_event: [],
  update_event: [],
  delete_event: [],
  all_assigment: [],
  assigment: [],
  attach_ambassador_to_event: [],
  dettach_ambassador_to_event: [],
  get_all_events_an_ambassador: [],
  mark_an_ambassador_for_an_event: []
}

export default {
  state, mutations, actions
}
