import Vue from 'vue'
import router from '../../router/index'

export default {
  get_all_events (context) {
    Vue.http.get('http://keatrading.co.ke:6002/v1/events/all').then(function (response) {
      context.commit('GET_ALL_EVENTS', response.body.data)
    })
  },
  post_event (context, data) {
    Vue.http.post('http://keatrading.co.ke:6002/v1/add/event', data).then(function (response) {
      router.push({
        name: 'eventspg'
      })
      Vue.data('all_events')
      context.dispatch('get_all_events', response.body.data)
    })
  },
  get_single_event (context, publicId) {
    Vue.http.get('http://keatrading.co.ke:6002/v1/event/' + publicId).then(function (response) {
      context.commit('GET_SINGLE_EVENT', response.body.data)
    })
  },
  update_event (context, publicId) {
    Vue.http.post('http://keatrading.co.ke:6002/v1/update/assigment/' + publicId).then(function (response) {
      router.push({
        name: 'createEvent'
      })
      Vue.data('get_all_events')
      context.dispatch('UPDATE_SINGLE_EVENT', response.body.data)
    })
  },
  delete_single_event (context) {
    Vue.http.delete('http://keatrading.co.ke:6002/v1/delete/event/public_id').then(function (response) {
      context.commit('DELETE_SINGLE_EVENT', response.body.data)
    })
  },
  get_all_assigment (context) {
    Vue.http.get('http://keatrading.co.ke:6002/v1/assigment/all').then(function (response) {
      context.commit('GET_ALL_ASSIGMENT', response.body.data)
    })
  },
  post_assigment (context, data) {
    Vue.http.post('http://keatrading.co.ke:6002/v1/add/assigment', data).then(function (response) {
      router.push({
        name: 'allassign'
      })
      Vue.data('all_assigment')
      context.dispatch('get_all_assigment', response.body.data)
    })
  },
  attach_ambassador_to_event (context, data) {
    Vue.http.post('http://keatrading.co.ke:6002/v1/attach', data).then(function (response) {
      router.push({
        name: 'eventspg'
      })
      Vue.data('attach_ambassador_to_event')
      context.dispatch('ATTACH_AMBASSADOR_TO_EVENT', response.body.data)
    })
  },
  dettach_ambassador_to_event (context, data) {
    Vue.http.post('http://keatrading.co.ke:6002/v1/dettach', data).then(function (response) {
      router.push({
        name: 'eventspg'
      })
      Vue.data('attach_ambassador_to_event')
      context.dispatch('DETTACH_AMBASSADOR_TO_EVENT', response.body.data)
    })
  },
  get_all_events_an_ambassador (context) {
    Vue.http.post('http://keatrading.co.ke:6002/v1/events/ambassodor/public_id').then(function (response) {
      router.push({
        name: 'eventspg'
      })
      Vue.data('get_all_events_an_ambassador')
      context.dispatch('GET_ALL_EVENTS_AN_AMBASSADOR', response.body.data)
    })
  },
  mark_an_ambassador_for_an_event (context, data) {
    Vue.http.post('http://keatrading.co.ke:6002/v1/mark/gone', data).then(function (response) {
      router.push({
        name: 'eventspg'
      })
      Vue.data('mark_an_ambassador_for_an_event')
      context.dispatch('MARK_AN_AMBASSADOR_FOR_AN_EVENT', response.body.data)
    })
  },
  post_feedback (context, data) {
    Vue.http.post('/src/components/feedback2', data).then(function (response) {
      router.push({
        name: 'feedback2'
      })
      Vue.data('feedback')
      context.dispatch('feedback2', response.body.data)
    })
  }
}
