export default {
  POST_SOCIAL_MEDIA (state, data) {
    state.post_social = data
  },
  GET_ALL_SOCIAL_MEDIA (state, data) {
    state.get_all_social_media = data
  },
  GET_SINGLE_SOCIAL_MEDIA (state, data) {
    state.get_single_social_media = data
  },
  POST_BASIC_ON_SOCIAL_MEDIA (state, data) {
    state.post_basic_on_social_media = data
  }
}
