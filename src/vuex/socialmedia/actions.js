import Vue from 'vue'
import router from '../../router/index'

export default {
  post_social_media (context, data) {
    Vue.http.post('http://keatrading.co.ke:6003/platforms/add', data).then(function (response) {
      router.push({
        name: 'allsocial'
      })
      Vue.data('post_social_media')
      context.dispatch('POST_SOCIAL_MEDIA', response.body.data)
    })
  },
  get_all_social_media (context) {
    Vue.http.get('http://keatrading.co.ke:6003/platforms/view').then(function (response) {
      context.commit('GET_ALL_SOCIAL_MEDIA', response.body.data)
    })
  },
  get_single_social_media (context, publicId) {
    Vue.http.get('http://keatrading.co.ke:6003/platforms/view/' + publicId).then(function (response) {
      context.commit('GET_SINGLE_SOCIAL_MEDIA', response.body.data)
    })
  },
  post_basic_on_social_media (context, data) {
    Vue.http.post('http://keatrading.co.ke:6003/user/platform/add', data).then(function (response) {
      router.push({
        name: 'ambassadorspg'
      })
      Vue.data('post_basic_on_social_media')
      context.dispatch('POST_BASIC_ON_SOCIAL_MEDIA', response.body.data)
    })
  }
}
