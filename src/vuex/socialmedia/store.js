import mutations from '././mutations'
import actions from '././actions'

const state = {
  post_social: [],
  get_all_social_media: [],
  get_single_social_media: []
}
export default {
  state, mutations, actions
}
