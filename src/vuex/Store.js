import Vue from 'vue'
import Vuex from 'vuex'
import EventStore from './events/store'
import AmbassadorStore from './ambassadors/store'
import SocialMediaStore from '././socialmedia/store'

Vue.config.productionTip = false
Vue.use(Vuex)
Vue.config.debug = true

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    EventStore,
    AmbassadorStore,
    SocialMediaStore
  },
  strict: debug
})
